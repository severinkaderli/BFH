# BFH
This repository contains links to all of my notes, homework solutions, and
projects that I created during my time at the BFH.

## Semester 1
* BTI7061 CSBas ([Repository](https://gitlab.com/severinkaderli/BTI7061-CSBas))
* BTI7051 OOP1 ([Repository](https://gitlab.com/severinkaderli/BTI7051-OOP1))
* BZG1151 DISMAT ([Repository](https://gitlab.com/severinkaderli/BZG1151-DISMAT))
* BZG3401 ENG1IN ([Repository](https://gitlab.com/severinkaderli/BZG3401-ENG1IN))

## Semester 2
* BTI7055 OOP2
  * Project: Qwixx ([Repository](https://gitlab.com/martyschaer/qwixx))
* BZG1152 LINALG
  * Presentation: Markov Chains ([Repository](https://gitlab.com/martyschaer/pagerank-with-markov-chains))
* BZG3402 ENG2IN ([Repository](https://gitlab.com/severinkaderli/BZG3402-ENG2IN))
* BZG3101 KOMM1D ([Repository](https://gitlab.com/severinkaderli/BZG3101-KOMM1D))

## Semester 3
* BTI7062 AlDa
  * [Homework 1](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-1)
  * [Homework 2](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-2)
  * [Homework 3](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-3)
  * [Homework 4](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-4)
  * [Homework 5](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-5)
  * [Homework 6](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-6)
  * [Homework 6](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-7)
  * [Homework 8](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-8)
  * [Homework 9](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-9)
* BZG2251.1 NWTEC1 ([Repository](https://gitlab.com/severinkaderli/BZG2251.1-NWTEC1))
* BZG2251.2 NWTEC1 ([Repository](https://gitlab.com/severinkaderli/BZG2251.2-NWTEC1))
* BZG3102 KOMM2D
  * Paper ([Repository](https://gitlab.com/martyschaer/kommunikation-arbeit))

## Semester 4
* BTI7081 SoED
  * Project: Patient Management System ([Repository](https://gitlab.com/severinkaderli/BTI7081-SoED-PMS))
* BZG2252.1 NWTEC2
  * Project: Matlab Exercises ([Repository](https://gitlab.com/severinkaderli/BZG2252.1-NWTEC2-Matlab-Series))
* BZG3403 ENG3IN
  * Presentation ([Repository](https://gitlab.com/severinkaderli/BZG3403-ENG3IN-Presentation))
* BTI7056 DB
  * [Homework 1](https://gitlab.com/severinkaderli/BTI7056-DB-Homework-1)
  * [Homework 2](https://gitlab.com/severinkaderli/BTI7056-DB-Homework-2)
  * [Homework 3](https://gitlab.com/severinkaderli/BTI7056-DB-Homework-3)
  * [Homework 4](https://gitlab.com/severinkaderli/BTI7056-DB-Homework-4)
  * [Homework 5](https://gitlab.com/severinkaderli/BTI7056-DB-Homework-5)
  * [Homework 6](https://gitlab.com/severinkaderli/BTI7056-DB-Homework-6)
  * [Homework 7](https://gitlab.com/severinkaderli/BTI7056-DB-Homework-7)
  * [Homework 8](https://gitlab.com/severinkaderli/BTI7056-DB-Homework-8)
  * [Homework 9](https://gitlab.com/severinkaderli/BTI7056-DB-Homework-9)
  * [Homework 10](https://gitlab.com/severinkaderli/BTI7056-DB-Homework-10)
  * [Homework 11](https://gitlab.com/severinkaderli/BTI7056-DB-Homework-11)
  * [Homework 12](https://gitlab.com/severinkaderli/BTI7056-DB-Homework-12)

## Semester 5
* BTI7064 AutLg ([Repository](https://gitlab.com/severinkaderli/BTI7064-AutLg))
* BTI7071 Tele ([Repository](https://gitlab.com/severinkaderli/BTI7071-Tele))
  * Project ([Repository](https://gitlab.com/severinkaderli/BTI7071-Tele-Project))
* BTI7301 Proj1
  * Project: tale, an experimental version control system ([Repository](https://gitlab.com/severinkaderli/tale))
* BTI7054 Web
  * Project: Web-Shop ([Repository](https://gitlab.com/severinkaderli/BTI7054-Web-Shop))

## Semester 6
* BTI7063 OpSys ([Repository](https://gitlab.com/severinkaderli/BTI7063-OpSys))
* BZG1154 WSTAT ([Repository](https://gitlab.com/severinkaderli/BZG1154-WSTAT))
* BTI7072 NetDS ([Repository](https://gitlab.com/severinkaderli/BTI7072-NetDS))
  * Project ([Repository](https://gitlab.com/severinkaderli/BTI7072-NetDS-Lab))
* BTI7251 MC1
  * Android ([Repository](https://gitlab.com/severinkaderli/BTI7251-MC1-Android))
  * Tinkerforge ([Repository](https://gitlab.com/severinkaderli/BTI7251-MC1-Tinkerforge))